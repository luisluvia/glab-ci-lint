# GitLab CI Lint

## Description

This is a simple tool to validate `.gitlab-ci.yml` files against the [GitLab CI Lint API](https://docs.gitlab.com/ee/api/lint.html).

## Installation

```
$ npm install -g glab-ci-lint
```

Or, for CI/CD:

```
$ npm install --save-dev glab-ci-lint
```

## Usage

### CLI

```
$ glab-ci-lint --help

Usage: glab-ci-lint [options] <file> <id>

Arguments:
  file                 GitLab CI config file
  id                   GitLab project ID

Options:
  -V, --version        output the version number
  -t, --token <token>  GitLab API token. See https://docs.gitlab.com/ee/api/rest/#authentication for more information
  -u, --url            GitLab URL (default: "https://gitlab.com")
  -h, --help           display help for command
```
