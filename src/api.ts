import kleur from 'kleur'
import { readFile } from 'fs/promises'
import { isError } from './utils.js'

interface LintResponse {
  valid: boolean
  errors: string[]
  warnings: string[]
}

interface NetworkErrorResponse {
  status: number
  statusText: string
}

export const lint = async (
  baseUrl: string,
  fileContents: string,
  projectId: string,
  token: string
): Promise<LintResponse | NetworkErrorResponse> => {
  const url = `${baseUrl}/api/v4/projects/${projectId}/ci/lint`

  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ content: fileContents })
  })

  if (response.ok) {
    const body = await response.json()

    return {
      valid: body.valid,
      errors: body.errors,
      warnings: body.warnings
    }
  } else {
    return {
      status: response.status,
      statusText: response.statusText
    }
  }
}

export const lintFile = async (
  baseUrl: string,
  filePath: string,
  projectId: string,
  token: string
) => {
  try {
    const fileContents = await readFile(filePath, 'utf-8')
    return lint(baseUrl, fileContents, projectId, token)
  } catch (error) {
    if (isError(error)) {
      switch (error.code) {
        case 'ENOENT':
          console.log(kleur.red(`File not found: ${filePath}`))
          process.exit(1)
          break
        case 'ERR_INVALID_ARG_TYPE':
          console.log(kleur.red(`Invalid file path: ${filePath}`))
          process.exit(1)
          break
        default:
          console.log(kleur.red(`Error reading file: ${error}`))
          process.exit(1)
          break
      }
    }
  }
}
