#! /usr/bin/env node

import { Command } from 'commander'
import kleur from 'kleur'
import { lintFile } from './api.js'

const program = new Command()

program
  .version('1.0.0')
  .argument('<file>', 'GitLab CI config file')
  .argument('<id>', 'GitLab project ID')
  .option(
    '-t, --token <token>',
    'GitLab API token. See https://docs.gitlab.com/ee/api/rest/#authentication for more information'
  )
  .option('-u, --url <url>', 'GitLab URL', 'https://gitlab.com')

program.parse()

const file = program.args[0]
const id = program.args[1]
const options = program.opts()

console.log('Linting GitLab CI config file...')

const results = await lintFile(options.url, file, id, options.token)

if (results && 'valid' in results) {
  if (results.valid) {
    console.log(`CI config is ${kleur.green('valid')}`)
  } else {
    console.log(`CI config is ${kleur.red('invalid')}`)
  }

  if (results.errors.length > 0) {
    console.log(`\n${results.errors.length} ${kleur.red('errors')} found\n`)
  }

  for (const error of results.errors) {
    console.log(`[${kleur.red('error')}] ${error}`)
  }

  if (results.warnings.length > 0) {
    console.log(`\n${results.warnings.length} ${kleur.yellow('warnings')} found\n`)
  }

  for (const warning of results.warnings) {
    console.log(`[${kleur.yellow('warning')}] ${warning}`)
  }

  if (results.valid) {
    process.exit(0)
  } else {
    process.exit(1)
  }
} else if (results) {
  console.log(kleur.red(`Network error: ${results.status} ${results.statusText}`))
  process.exit(1)
}
